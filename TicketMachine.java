/**
 * TicketMachine models a naive ticket machine that issues
 * flat-fare tickets.
 * The price of a ticket is specified via the constructor.
 * It is a naive machine in the sense that it trusts its users
 * to insert enough money before trying to print a ticket.
 * It also assumes that users enter sensible amounts.
 *
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 */
public class TicketMachine
{
    // The price of a ticket from this machine.
    private int price;
    // The amount of money entered by a customer so far.
    private int balance;
    // The total amount of money collected by this machine.
    private int total;

    /**
     * Create a machine that issues tickets of the given price.
     * Note that the price must be greater than zero, and there
     * are no checks to ensure this.
     */
    public TicketMachine(int ticketCost) throws Exception
    {
        if (ticketCost<=0){
            throw new Exception("Prix n�gatif ou egal � 0 !");
    
    }
        price = ticketCost;
        balance = 0;
        total = 0;
    }

    /**
     * Constructeur par d�faut (sans param�tre)
     */
    public TicketMachine() {
        price = 50; // Le prix du ticket est fix� de mani�re arbitraire
        balance = 0;
        total = 0;
    }

    /**
     * Return the price of a ticket.
     */
    public int getPrice()
    {
        return price;
    }

    /**
     * Return the amount of money already inserted for the
     * next ticket.
     */
    public int getBalance()
    {
        return balance;
    }
    
    /**
     * Return the total
     */
    public int getTotal()
    {
        return total;
    }

    /**
     * Receive an amount of money in cents from a customer.
     */
    public void insertMoney(int amount) throws Exception
    {
        if(amount<0){
            throw new Exception("montant ins�r� n�gatif !!");
            
    }
    balance = balance + amount;
    // balance += amount ; cf. Coursera 
    }
    
    /**
     * Met la balance � 0.
     */
    public void refundMoney()
    { 
        balance=0;
    }
    
     /**
     * Reset total.
     */
    public void resetTotal()
    { 
        total=0;
    }

    /**
     * Print a ticket.
     * Update the total collected and
     * reduce the balance to zero.
     */
    public void printTicket()
    {
        if(balance>=price){
            // Simulate the printing of a ticket.
            System.out.println("##################");
            System.out.println("# The BlueJ Line");
            System.out.println("# Ticket");
            System.out.println("# " + price + " cents.");
            System.out.println("##################");
            System.out.println();

            // Update the total collected with the balance.
            total = total + price;
            balance -= price;
        }else{
            System.out.println("il manque" + (price-balance));
        }
    }
}
